<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:ns="http://xml.bruzgys.eu/patisserie"
                xmlns:xhtml="http://www.w3.org/1999/xhtml"
                version="1.0">

  <xsl:template match='/'>
    <html>
      <head>
        <title>
          <xsl:value-of select='/ns:patisserie/ns:meta/ns:title'/>
        </title>
      </head>
      <body>
        <xsl:apply-templates/>
      </body>
    </html>
  </xsl:template>

  <xsl:template match='ns:meta'></xsl:template>

  <xsl:template match='ns:authors'>
    <table>
      <tr>
        <th></th>
        <th>Name</th>
        <th>Birth</th>
      </tr>
      <xsl:apply-templates select='ns:author'>
        <xsl:sort order='descending' select='ns:author/ns:name'/>
      </xsl:apply-templates>
    </table>
  </xsl:template>

  <xsl:template match='ns:author'>
    <tr>
      <td>
        <xsl:number/>
      </td>
      <td>
        <xsl:value-of select='ns:name'/>
      </td>
      <td>
        <xsl:value-of select='ns:birth'/>
      </td>
    </tr>
  </xsl:template>

  <xsl:template match='ns:recipes'>
    <table>
      <tr>
        <th></th>
        <th>Author</th>
        <th>Title</th>
        <th>Created</th>
        <th>Price</th>
        <th>Ingredients</th>
      </tr>
      <xsl:apply-templates select='ns:recipe'/>
    </table>
  </xsl:template>

  <xsl:template match='ns:recipe'>
    <tr>
      <td>
        <xsl:value-of select='position()'/>
      </td>
      <td>
        <xsl:variable name='id' select='ns:author'/>
        <xsl:value-of select='/ns:patisserie/ns:authors/ns:author[@id = $id]/ns:name'/>
      </td>
      <td>
        <xsl:value-of select='ns:title'/>
      </td>
      <td>
        <xsl:value-of select='ns:created'/>
      </td>
      <td>
        <xsl:choose>
          <xsl:when test='ns:price/@currency != ""'>
            <xsl:value-of select='concat(ns:price, " ", ns:price/@currency)'/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select='concat(ns:price, " LTL")'/>
          </xsl:otherwise>
        </xsl:choose>
      </td>
      <td>
        <xsl:for-each select='ns:ingredients'>
          <xsl:value-of select='ns:ingredient'/>
          <xsl:if test='position() != last()'>
            <xsl:text> </xsl:text>
          </xsl:if>
        </xsl:for-each>
      </td>
      <td></td>
    </tr>
  </xsl:template>

</xsl:stylesheet>